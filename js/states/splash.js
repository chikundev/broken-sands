// chikun :: 2014
// Splash state


states.splash = {

    create: function () {

        this.timer = 0;
        this.blipPlayed = false;

        obj.logo = makeSprite(gfx.splash, 0, 0);
        stage.addChild(obj.logo);

        obj.logo.position.x = (renderer.width  - 640) / 2;
        obj.logo.position.y = (renderer.height - 360) / 2;

    },

    update: function (dt) {

        obj.logo.alpha = 1 - (Math.abs(this.timer - 0.5) * 2);

        this.timer += dt;

        if (this.timer >= 1) {
            state.changeTo("play");
        } else if (this.timer >= 0.5 && !this.blipPlayed) {
            this.blipPlayed = true;
            //document.getElementById("blip").play();
        }

        if (this.timer >= 0.75) {
            obj.logo.position.x += 256 * dt;
        }

    },

    kill: function () {

        stage.removeChildren();

        obj = { };

    }
};
