gfx = { };

var loader, images;

images = [
    "/gfx/actionPrompt.png",
    "/gfx/androidBtnA.png",
    "/gfx/splash.png"
];

// New image loader
loader = new PIXI.AssetLoader(images);

// Execute the loading function
loader.onComplete = gameStart;

// Performed upon each image loading
loader.onProgress = function (x) {

    // Determine URL of loaded image
    var name = x.texture.baseTexture.imageUrl;

    // Get name of image
    name = name.slice(5, -4);

    // Add image to gfx table
    gfx[name] = x.texture;

};

// Start loading assets
loader.load();


// Create new sprite and return it
function makeSprite(texture, x, y, anchorX, anchorY, scaleX, scaleY) {

    // New sprite
    var tmpSprite = new PIXI.Sprite(texture);

    // Set position if provided
    tmpSprite.position.x = (x || 0);
    tmpSprite.position.y = (y || 0);

    // Set anchor if provided
    tmpSprite.anchor.x = (anchorX || 0);
    tmpSprite.anchor.y = (anchorY || 0);

    // Set scale if provided
    tmpSprite.scale.x = (scaleX || 1);
    tmpSprite.scale.y = (scaleY || 1);

    // Return new sprite
    return (tmpSprite);

}

