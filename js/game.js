// chikun :: 2014
// Contains all game functions


// Container for all game functions
var game = { };


// Performed at game load
game.load = function () {

    var container = document.getElementById("container");

    // Create a new instance of the PIXI stage
    stage = new PIXI.Stage(0x000000);

    // Create a renderer instance
    renderer = new PIXI.autoDetectRenderer(container.clientWidth,
                                          container.clientHeight);

    // Attach the renderer to the container div
    container.appendChild(renderer.view);

    requestAnimFrame(game.update);

    // Set date for delta time
    lastUpdate = Date.now();

    // Create sprite
    /*var newSprite = new makeSprite(gfx.splash, 0, 0);

    stage.addChild(newSprite);*/

    state.load(state.current);

};


// Performed on game update
game.update = function () {

    requestAnimFrame(game.update);

    // Calculate delta time and update lastUpdate
    var now = Date.now();
    var dt = (now - lastUpdate) / 1000;
    lastUpdate = now;

    state.update(dt);

    // Draw game
    game.draw();

};


// Performed to draw game
game.draw = function () {

    // Actually render the stage
    renderer.render(stage);

};
