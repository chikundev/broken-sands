// chikun :: 2014
// Require libraries then run game


// Initialise important global variables
var stage, renderer, lastUpdate, gfx;

// Create objects variable
var obj = { };

// Create states variable
var states = { };

var libraries = [
    "game",
    "input",
    "stateManager"
];

var stateFiles = [
    "states/splash",
    "states/template"
];


requirejs(libraries, function() {
    requirejs(stateFiles, function() {
        requirejs(["loadGFX"]);
    });
});


// Performed on game load
function gameStart() {
    input.init();
    game.load();
}
